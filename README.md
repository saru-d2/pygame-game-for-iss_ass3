# pygame game for ISS_ass3

Another ISS assignment...

This game is, in its essence a road crossing game, like crossy road.
This game was coded in python using the pygame library.

Instructions: get to the other end

Controls:
    Player one: up, down, left, right
    Player two: w, a, s, d
    
Scoring:
    Finishing a level gives you 100 points
    Moving past a moving obstacle gives you 10 points
    Moving past a stationary obstacle gives you 5 points
    You lose 1 point every second, so you have to be quick
    
Functional details:
    The game has 5 moving, and five stationary obstacles.
    There are 2 players in the game, only one of them play at a time
    